FROM jboss/keycloak:5.0.0

VOLUME /opt/jboss/keycloak/standalone/data


RUN mkdir $JBOSS_HOME/.cacerts/
ADD win.duke.edu.pem $JBOSS_HOME/.cacerts/

RUN cp /etc/pki/ca-trust/extracted/java/cacerts $JBOSS_HOME/.cacerts/

RUN chmod 644 $JBOSS_HOME/.cacerts/cacerts
RUN ls -alh $JBOSS_HOME/.cacerts/cacerts

RUN $JAVA_HOME/bin/keytool -keystore $JBOSS_HOME/.cacerts/cacerts \
      -storepass changeit -noprompt -import -alias win.duke.edu \
      -file $JBOSS_HOME/.cacerts/win.duke.edu.pem
